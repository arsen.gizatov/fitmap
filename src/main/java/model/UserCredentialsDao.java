package model;

import databean.UserCredentials;
import org.genericdao.ConnectionPool;
import org.genericdao.DAOException;
import org.genericdao.GenericDAO;

public class UserCredentialsDao extends GenericDAO<UserCredentials> {
    public UserCredentialsDao(ConnectionPool pool, String tableName) throws DAOException {
        super(UserCredentials.class, tableName, pool);
    }

    public static final String TABLE_NAME = "user_credentials";
}
