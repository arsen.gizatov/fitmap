package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import model.Model;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class Controller extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    public void init() throws ServletException {
        Model model = new Model(getServletConfig());

        Action.add(new UserAllGymsAction(model));
        Action.add(new UserDepositMoneyAction(model));
        Action.add(new UserRegistrationAction(model));
        Action.add(new UserLoginAction(model));
        Action.add(new AdminMainPageAction(model));
        Action.add(new LogoutAdminAction());
        Action.add(new AdminLoginAction(model));
        Action.add(new UserMainPageAction(model));
        Action.add(new LogoutUserAction());
        Action.add(new UserSchedulePageAction(model));
        Action.add(new UserDeleteClassAction(model));
        Action.add(new UserTakeClassAction(model));
        Action.add(new AdminCreateClassAction(model));
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nextPage = performTheAction(request);
        sendToNextPage(nextPage, request, response);
    }

    // methods
    private String performTheAction(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        String servletPath  = request.getServletPath();
        String action       = getActionName(servletPath);

        // checking the registration of a user (do they exist in the session)

        if (action.contains("user") && session.getAttribute(Session.USER) != null)
            return Action.perform(action, request);
        if (action.contains("admin") && session.getAttribute(Session.ADMIN) != null)
            return Action.perform(action, request);


        if (action.equals(Pages.USER_LOGIN))
            return Action.perform(Pages.USER_LOGIN, request);
        if (action.equals(Pages.USER_REGISTRATION))
            return Action.perform(Pages.USER_REGISTRATION, request);
        if (action.equals(Pages.ADMIN_LOGIN))
            return Action.perform(Pages.ADMIN_LOGIN, request);

        return "controller-error.jsp";
    }

    private String getActionName(String servletPath) {
        int begin = servletPath.lastIndexOf('/');
        return servletPath.substring(begin + 1);
    }

    /**
     * Send to specified link by own logic
     * @param nextPage name of next link
     * @param request request servlet
     * @param response response for client
     */
    private void sendToNextPage(String nextPage,
                                HttpServletRequest request,
                                HttpServletResponse response) throws ServletException, IOException {
        if (nextPage == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND,
                    request.getServletPath());
            return;
        }

        if (nextPage.endsWith(".do")) {
            response.sendRedirect(nextPage);
            return;
        }


        if (nextPage.endsWith(".jsp")) {
            RequestDispatcher dispatcher = request.getRequestDispatcher(nextPage);
            dispatcher.forward(request, response);
            return;
        }

        if (nextPage.equals("ImageView")) {
            RequestDispatcher d = request.getRequestDispatcher("/ViewImageServlet");
            d.forward(request, response);
            return;
        }

        throw new ServletException(Controller.class.getName()
                + ".sendToNextPage(\"" + nextPage + "\"): invalid extension. ");
    }

}
