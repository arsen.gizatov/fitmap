package formbean;

import lombok.Data;
import org.formbeanfactory.FieldOrder;
import org.formbeanfactory.FormBean;
import org.formbeanfactory.InputType;
import org.formbeanfactory.Label;

@Data
@FieldOrder("name,surname,city,email,password,repeatPassword")
public class UserRegistrationForm extends FormBean {
    private String name;
    private String surname;
    private String city;
    private String email;
    private String password;
    private String repeatPassword;

    @Label("Password")
    @InputType("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @Label("Repeat password")
    @InputType("password")
    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    @Override
    public void validate() {
        super.validate();

        if (hasValidationErrors())
            return;

        if (!email.matches(".*[@].*"))
            addFieldError("email", "Email format is invalid!");

        if (!password.equals(repeatPassword)) {
            addFieldError("repeatPassword", "Passwords is not same!");
        }
    }
}
