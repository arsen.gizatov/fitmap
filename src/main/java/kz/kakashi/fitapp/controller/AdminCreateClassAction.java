package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import databean.AdminUsers;
import databean.Schedules;
import formbean.AdminCreateClassForm;
import model.Model;
import model.UserSchedulesDao;
import org.formbeanfactory.FormBeanFactory;
import org.formbeanfactory.FormBeanFactoryException;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AdminCreateClassAction extends Action {
    private FormBeanFactory<AdminCreateClassForm> formFormBeanFactory = new FormBeanFactory<>(AdminCreateClassForm.class);
    private UserSchedulesDao userSchedulesDao;

    public AdminCreateClassAction(Model model) {
        this.userSchedulesDao = model.getUserSchedulesDao();
    }

    @Override
    public String getName() {
        return Pages.ADMIN_CREATE_CLASSES;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        HttpSession session = request.getSession();
        request.setAttribute("form", new AdminCreateClassForm());
        return Pages.JSP_ADMIN_CREATE_CLASSES;
    }

    @Override
    public String performPost(HttpServletRequest request) {
        HttpSession session = request.getSession();
        AdminUsers currentAdmin = (AdminUsers) session.getAttribute(Session.ADMIN);
        try {
            AdminCreateClassForm form = formFormBeanFactory.create(request);
            request.setAttribute("form", form);
            if (form.hasValidationErrors())
                return Pages.JSP_ADMIN_CREATE_CLASSES;

            Schedules schedule = new Schedules();
            schedule.setGym(currentAdmin.getName());
            schedule.setAddress(currentAdmin.getAddress());
            schedule.setTypeOfSport(form.getTypeOfClass());
            schedule.setDate(form.getDay());
            schedule.setTime(form.getTime());
            schedule.setPrice(form.getPriceAsDouble());

            userSchedulesDao.create(schedule);
            request.setAttribute("success_message", "New class created successfully!!!");

            return Pages.JSP_ADMIN_CREATE_CLASSES;
        } catch (RollbackException | FormBeanFactoryException exception) {
            request.setAttribute("error", exception.getMessage());
            return Pages.JSP_ADMIN_CREATE_CLASSES;
        }
    }
}
