package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import databean.User;
import formbean.UserDepositForm;
import model.Model;
import model.UserDao;
import org.formbeanfactory.FormBeanFactory;
import org.formbeanfactory.FormBeanFactoryException;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;

public class UserDepositMoneyAction extends Action{
    private FormBeanFactory<UserDepositForm> formFormBeanFactory = new FormBeanFactory<>(UserDepositForm.class);
    private UserDao userDao;

    public UserDepositMoneyAction(Model model) {
        this.userDao = model.getUserDao();
    }

    @Override
    public String getName() {
        return Pages.USER_DEPOSIT_MONEY;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        request.setAttribute("form", new UserDepositForm());
        return Pages.JSP_USER_DEPOSIT_MONEY;
    }

    @Override
    public String performPost(HttpServletRequest request) {
        try {
            UserDepositForm form = formFormBeanFactory.create(request);
            request.setAttribute("form", form);
            if (form.hasValidationErrors())
                return Pages.JSP_USER_DEPOSIT_MONEY;

            User user = (User) request.getSession().getAttribute(Session.USER);
            User userFromDb = userDao.read(user.getId());

            double money = form.getMoneyAsDouble();
            userFromDb.setBalance(userFromDb.getBalance() + money);
            userDao.update(userFromDb);

            request.setAttribute("success_message", "Balance updated successfully!");
            return Pages.JSP_USER_DEPOSIT_MONEY;
        } catch (FormBeanFactoryException | RollbackException e) {
            request.setAttribute("error", e.getMessage());
            return Pages.JSP_USER_DEPOSIT_MONEY;
        }

    }
}
