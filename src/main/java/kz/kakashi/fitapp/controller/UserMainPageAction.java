package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import databean.MySchedules;
import databean.User;
import model.Model;
import model.MySchedulesDao;
import model.UserDao;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserMainPageAction extends Action {

    private MySchedulesDao mySchedulesDao;
    private UserDao userDao;

    public UserMainPageAction(Model model) {
        this.mySchedulesDao = model.getMySchedulesDao();
        this.userDao = model.getUserDao();
    }

    @Override
    public String getName() {
        return Pages.USER_PAGE;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();
            User user = (User) session.getAttribute(Session.USER);
            User userFromDb = userDao.read(user.getId());
            MySchedules[] mySchedules = mySchedulesDao.match();
            request.setAttribute("user", userFromDb);
            request.setAttribute("schedules", mySchedules);
            return Pages.JSP_USER_PAGE;
        } catch (RollbackException exception) {
            request.setAttribute("error", exception.getMessage());
            return Pages.JSP_USER_PAGE;
        }
    }
}
