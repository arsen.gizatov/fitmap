package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LogoutUserAction extends Action{
    @Override
    public String getName() {
        return Pages.USER_LOGOUT;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute(Session.USER);
        return Pages.USER_LOGIN;
    }
}
