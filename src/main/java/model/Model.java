package model;

import lombok.Data;
import lombok.Getter;
import org.genericdao.ConnectionPool;
import org.genericdao.DAOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

@Data
@Getter
public class Model {
    private static final String JDBC_DRIVER_NAME = "jdbcDriverName";
    private static final String JDBC_URL = "jdbcURL";

    private UserDao            userDao;
    private UserCredentialsDao userCredentialsDao;
    private UserSchedulesDao userSchedulesDao;
    private AdminCredentialsDao adminCredentialsDao;
    private AdminUsersDao adminUsersDao;
    private MySchedulesDao mySchedulesDao;

    public Model(ServletConfig config) throws ServletException {
        try {
            String jdbcDriver = config.getInitParameter(JDBC_DRIVER_NAME);
            String jdbcUrl    = config.getInitParameter(JDBC_URL);

            ConnectionPool pool = new ConnectionPool(jdbcDriver, jdbcUrl);

            this.userCredentialsDao = new UserCredentialsDao(pool, UserCredentialsDao.TABLE_NAME);
            this.userDao = new UserDao(pool, UserDao.TABLE_NAME);
            this.userSchedulesDao = new UserSchedulesDao(pool, UserSchedulesDao.TABLE_NAME);
            this.adminCredentialsDao = new AdminCredentialsDao(pool, AdminCredentialsDao.TABLE_NAME);
            this.adminUsersDao = new AdminUsersDao(pool, AdminUsersDao.TABLE_NAME);
            this.mySchedulesDao = new MySchedulesDao(pool, MySchedulesDao.TABLE_NAME);
        } catch (DAOException ex) {
            throw new ServletException(ex);
        }
    }
}
