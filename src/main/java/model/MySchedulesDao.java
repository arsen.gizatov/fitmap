package model;

import databean.MySchedules;
import org.genericdao.ConnectionPool;
import org.genericdao.DAOException;
import org.genericdao.GenericDAO;

public class MySchedulesDao extends GenericDAO<MySchedules> {
    public MySchedulesDao(ConnectionPool connectionPool, String tableName) throws DAOException {
        super(MySchedules.class, tableName, connectionPool);
    }

    public static final String TABLE_NAME = "my_schedules";
}
