package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import databean.AdminUsers;
import databean.MySchedules;
import databean.Schedules;
import databean.User;
import formbean.IdForm;
import model.*;
import org.formbeanfactory.FormBeanFactory;
import org.formbeanfactory.FormBeanFactoryException;
import org.genericdao.MatchArg;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserTakeClassAction extends Action {
    private FormBeanFactory<IdForm> formFormBeanFactory = new FormBeanFactory<>(IdForm.class);
    private AdminUsersDao adminUsersDao;
    private UserSchedulesDao userSchedulesDao;
    private MySchedulesDao mySchedulesDao;
    private UserDao userDao;

    public UserTakeClassAction(Model model) {
        this.adminUsersDao = model.getAdminUsersDao();
        this.userSchedulesDao = model.getUserSchedulesDao();
        this.userDao = model.getUserDao();
        this.mySchedulesDao = model.getMySchedulesDao();
    }

    @Override
    public String getName() {
        return Pages.USER_TAKE_CLASS;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        return Pages.JSP_SCHEDULE;
    }

    @Override
    public String performPost(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User currentUser = (User) session.getAttribute(Session.USER);
        try {
            IdForm form = formFormBeanFactory.create(request);
            request.setAttribute("form", form);
            if (form.hasValidationErrors())
                return Pages.JSP_SCHEDULE;

            //first should to create check for my schedules table if exist then do not validate
            Schedules schedule = userSchedulesDao.read(form.getIdAsInt());
            request.setAttribute("schedules", userSchedulesDao.match());
            MySchedules[] mySchedules = mySchedulesDao.match(MatchArg.equals("scheduleId", schedule.getId()));
            if (mySchedules.length != 0) {
                request.setAttribute("error_message", "This class already taken!!");
                return Pages.JSP_SCHEDULE;
            }
            // then check price and balance
            String gymName = schedule.getGym();
            double price = schedule.getPrice();
            User user = userDao.read(currentUser.getId());
            double balance = user.getBalance();
            if (price > balance) {
                request.setAttribute("error_message", String.format("Your balance %.2f is not enough to take this course", balance));
                return Pages.JSP_SCHEDULE;
            }

            // if all ok, then update info
            AdminUsers adminUsers = adminUsersDao.match(MatchArg.equals("name", gymName))[0];
            adminUsers.setDeposit(adminUsers.getDeposit() + price);
            adminUsersDao.update(adminUsers);

            user.setBalance(balance - price);
            userDao.update(user);

            MySchedules newSchedule = new MySchedules();
            newSchedule.setScheduleId(schedule.getId());
            newSchedule.setGym(schedule.getGym());
            newSchedule.setAddress(schedule.getAddress());
            newSchedule.setTypeOfSport(schedule.getTypeOfSport());
            newSchedule.setDate(schedule.getDate());
            newSchedule.setTime(schedule.getTime());

            mySchedulesDao.create(newSchedule);

            request.setAttribute("success_message", "Schedule added successfully!!!");
            return Pages.JSP_SCHEDULE;
        } catch (RollbackException | FormBeanFactoryException ex) {
            request.setAttribute("error", ex.getMessage());
            return Pages.JSP_SCHEDULE;
        }
    }
}
