<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="common.Pages" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="resources/style.css">
	<title>Document</title>
</head>

<style>
	.all_gyms {
		display: flex;
		flex-direction: column;
	}
	.one_gym{
		position: relative;
		width: 800px;
		height: 250px;
		margin-bottom: 30px;
		display: flex;
		flex-direction: row;
		justify-content: space-between;
		background: #FFDEAD;
		margin-left: 40px;
		padding: 20px;
	}
	.info{
		margin-left: 330px;
		width: 50%;
		top: 10px;
		position: absolute;
	}
	#gym_photo{
		margin-right: 300px;
	}
</style>
<body>
	<aside>
		<img src="resources/quote.jpg" alt="quote" class="logo">
		<div class="menu">
			<a href="<%= Pages.USER_PAGE %>">MAIN PAGE</a>
			<a href="<%= Pages.USER_ALL_GYMS %>"><h4>ALL GYMS</h4></a>
			<a href="<%= Pages.SCHEDULE %>"><h4>SCHEDULE</h4></a>
			<a href="<%= Pages.USER_DEPOSIT_MONEY %>"><h4>DEPOSIT MONEY</h4></a>
			<a href="<%= Pages.USER_LOGOUT %>"><h4>LOGOUT</h4></a>
		</div>
	</aside>
	
	<main>
		<div class="all_gyms">
			<p style="color: red"><c:out value="${requestScope.error}"/></p> <br>
			<c:forEach var="admin" items="${requestScope.admins}">
				<div class="one_gym">
					<img id="gym_photo" alt="myphoto" src="resources/fitnes.jpg" width="250px" height="230px">
					<form class="info">
						<label for="fname">Gym Name</label>
						<input type="text" id="fname" name="fname" value="${admin.name}">
						<label for="fsurname">Address</label>
						<input type="text" id="fsurname" name="fsurname" value="${admin.address}">
						<label for="fcity">Types of classes</label>
						<input type="text" id="fcity" name="fcity" value="${admin.type}">
						<label for="fbalance">Contact</label>
						<input type="text" id="fbalance" name="fbalance" value="${admin.contact}">
					</form>
				</div>
			</c:forEach>
		</div>
	</main>
</body>
</html>