package model;

import databean.AdminCredentials;
import org.genericdao.ConnectionPool;
import org.genericdao.DAOException;
import org.genericdao.GenericDAO;

public class AdminCredentialsDao extends GenericDAO<AdminCredentials> {
    public AdminCredentialsDao(ConnectionPool pool, String tableName) throws DAOException {
        super(AdminCredentials.class, tableName, pool);
    }

    public static final String TABLE_NAME = "admin_credentials";
}
