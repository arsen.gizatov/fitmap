package formbean;

import lombok.Data;
import org.formbeanfactory.FieldOrder;
import org.formbeanfactory.FormBean;

@FieldOrder("typeOfClass,day,time,price")
@Data
public class AdminCreateClassForm extends FormBean {
    private String typeOfClass;
    private String day;
    private String time;
    private String price;

    public double getPriceAsDouble() {
        return Double.parseDouble(price);
    }

    @Override
    public void validate() {
        super.validate();

        if (hasValidationErrors())
            return;

        if (!time.contains(":"))
            addFieldError("time", "Invalid format!");

        try {
            Double.parseDouble(price);
        } catch (NumberFormatException ex) {
            addFieldError("price", "Price should be a decimal number");
        }
    }
}
