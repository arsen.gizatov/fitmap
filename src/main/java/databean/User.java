package databean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.genericdao.PrimaryKey;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PrimaryKey("id")
public class User {
    private int id;
    private String name;
    private String surname;
    private String city;
    private String email;
    private double balance;
}
