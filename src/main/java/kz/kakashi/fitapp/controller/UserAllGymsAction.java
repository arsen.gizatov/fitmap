package kz.kakashi.fitapp.controller;

import common.Pages;
import databean.AdminUsers;
import model.AdminUsersDao;
import model.Model;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;

public class UserAllGymsAction extends Action {
    private AdminUsersDao adminUsersDao;

    public UserAllGymsAction(Model model) {
        this.adminUsersDao = model.getAdminUsersDao();
    }

    @Override
    public String getName() {
        return Pages.USER_ALL_GYMS;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        try {
            AdminUsers[] adminUsers = adminUsersDao.match();
            request.setAttribute("admins", adminUsers);
            return Pages.JSP_USER_ALL_GYMS;
        } catch (RollbackException exception) {
            request.setAttribute("error", exception.getMessage());
            return Pages.JSP_USER_ALL_GYMS;
        }
    }
}
