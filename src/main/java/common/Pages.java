package common;

public class Pages {
    public static final String USER_LOGIN           = "user-login.do";
    public static final String USER_PAGE            = "user-page.do";
    public static final String USER_REGISTRATION    = "user-registration.do";
    public static final String USER_LOGOUT          = "user-logout.do";
    public static final String SCHEDULE             = "user-schedule.do";
    public static final String USER_TAKE_CLASS      = "user-take-class.do";
    public static final String USER_DELETE_CLASS    = "user-delete-class.do";
    public static final String USER_DEPOSIT_MONEY   = "user-deposit-money.do";
    public static final String USER_ALL_GYMS        = "user-all-gyms.do";

    public static final String ADMIN_LOGIN          = "admin-login.do";
    public static final String ADMIN_PAGE           = "admin.do";
    public static final String ADMIN_LOGOUT         = "admin-logout.do";
    public static final String ADMIN_CREATE_CLASSES = "admin-create-classes.do";

    public static final String JSP_USER_LOGIN           = "user_login.jsp";
    public static final String JSP_USER_PAGE            = "user_page.jsp";
    public static final String JSP_USER_REGISTRATION    = "user_registration.jsp";
    public static final String JSP_SCHEDULE             = "user_schedule.jsp";
    public static final String JSP_USER_DEPOSIT_MONEY   = "user_deposit_money.jsp";
    public static final String JSP_USER_ALL_GYMS        = "user_all_gyms.jsp";

    public static final String JSP_ADMIN_LOGIN          = "admin_login.jsp";
    public static final String JSP_ADMIN_PAGE           = "admin.jsp";
    public static final String JSP_ADMIN_CREATE_CLASSES = "admin_create_classes.jsp";
}
