<%@ page import="common.Pages" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="resources/style.css">
	<title>Document</title>
</head>
<body>
	<aside>
		<img src="resources/quote.jpg" alt="quote" class="logo">
		<div class="menu">
			<a href="<%= Pages.USER_PAGE %>">MAIN PAGE</a>
			<a href="<%= Pages.USER_ALL_GYMS %>"><h4>ALL GYMS</h4></a>
			<a href="<%= Pages.SCHEDULE %>"><h4>SCHEDULE</h4></a>
			<a href="<%= Pages.USER_DEPOSIT_MONEY %>"><h4>DEPOSIT MONEY</h4></a>
			<a href="<%= Pages.USER_LOGOUT %>"><h4>LOGOUT</h4></a>
		</div>
	</aside>
	
	<main>
		<div class="user_image">
			<img alt="myphoto" src="resources/user.jpeg" width="250px" height="230px">
			<h4><c:out value="${sessionScope.current_user.email}"/></h4>
		</div>

		<div class="user_info">
			<h3>My Info</h3>
			<form>
				<label for="fname">Name</label>
  				<input type="text" id="fname" name="fname" value="${requestScope.user.name}" readonly>
  				<label for="fsurname">Surname</label>
  				<input type="text" id="fsurname" name="fsurname" value="${requestScope.user.surname}" readonly>
  				<label for="fcity">City</label>
  				<input type="text" id="fcity" name="fcity" value="${requestScope.user.city}" readonly>
  				<label for="fbalance">Balance</label>
  				<input type="text" id="fbalance" name="fbalance" value="$${requestScope.user.balance}" readonly>
			</form>
		</div>

		<p style="color: red"><c:out value="${requestScope.error}"/></p> <br>
		<c:forEach var="error" items="${form.formErrors}">
			<h3 style="color: red;"> ${error} </h3>
		</c:forEach>

		<div class="user_shedule" style="padding: 40px 20px;">
			<h3>My Schedule</h3>
			<table style="width: 90%;">
				<tr>
					<th>Залдың аты</th>
					<th>Спорт түрі</th>
					<th>Адрес</th>
					<th>Күні</th>
					<th>Уақыт</th>
					<th></th>
				</tr>
				<c:forEach var="schedule" items="${requestScope.schedules}">
					<tr>
						<td>${schedule.gym}</td>
						<td>${schedule.typeOfSport}</td>
						<td>${schedule.address}</td>
						<td>${schedule.date}</td>
						<td>${schedule.time}</td>
						<td>
							<form action="<%= Pages.USER_DELETE_CLASS %>" method="POST">
								<input type="hidden" name="id" value="${schedule.id}">
								<button class="btn btn-primary" type="submit">Retake class</button>
							</form>
						</td>
					</tr>
				</c:forEach>
			</table>
			<c:if test="${requestScope.success_message != null}">
				<div class="alert alert-success" role="alert">
					<c:out value=" ${requestScope.success_message} " />
				</div>
			</c:if>
		</div>
	</main>
</body>
</html>