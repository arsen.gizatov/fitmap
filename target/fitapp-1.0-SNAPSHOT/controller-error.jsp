<%@ page import="common.Pages" %><%--
  Created by IntelliJ IDEA.
  User: arsen
  Date: 06.04.2021
  Time: 15:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Action Error Page</title>
</head>

<body>

<h2>Controller Stale Session Error</h2>

<p>
    You are attempting to run an action that requires you
    to be logged in.  Perhaps you were logged in, but
    inactive for so long that your session timed out.
    You will need to login again.
</p>
<p>
    Click <a href="<%= Pages.USER_LOGIN %>">here</a> to login.
</p>

</body>
</html>
