package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import databean.MySchedules;
import databean.Schedules;
import databean.User;
import formbean.IdForm;
import model.*;
import org.formbeanfactory.FormBeanFactory;
import org.formbeanfactory.FormBeanFactoryException;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserDeleteClassAction extends Action {

    private FormBeanFactory<IdForm> formFormBeanFactory = new FormBeanFactory<>(IdForm.class);
    private MySchedulesDao mySchedulesDao;
    private UserSchedulesDao userSchedulesDao;

    public UserDeleteClassAction(Model model) {
        this.mySchedulesDao = model.getMySchedulesDao();
        this.userSchedulesDao = model.getUserSchedulesDao();
    }

    @Override
    public String getName() {
        return Pages.USER_DELETE_CLASS;
    }

    @Override
    public String performPost(HttpServletRequest request) {
        try {
            IdForm form = formFormBeanFactory.create(request);
            request.setAttribute("form", form);
            if (form.hasValidationErrors())
                return Pages.JSP_USER_PAGE;

            Schedules[] allSchedules = userSchedulesDao.match();
            request.setAttribute("schedules", allSchedules);
            MySchedules mySchedules = mySchedulesDao.read(form.getIdAsInt());
            if (mySchedules == null) {
                form.addFormError("This schedule is not represented into your table");
                return Pages.JSP_USER_PAGE;
            }

            mySchedulesDao.delete(mySchedules.getId());
            request.setAttribute("success_message", "Schedule removed successfully!!!");
            return Pages.USER_PAGE;
        } catch (RollbackException | FormBeanFactoryException exception) {
            request.setAttribute("error", exception.getMessage());
            return Pages.JSP_USER_PAGE;
        }
    }
}
