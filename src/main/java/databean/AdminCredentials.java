package databean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.genericdao.PrimaryKey;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PrimaryKey("gymId")
public class AdminCredentials {
    private int gymId;
    private String password;
}
