<%@ page import="common.Pages" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="resources/style.css">
	<title>Document</title>
</head>
<style>
	.deposit_money{
		top: 30px;
		width: 350px;
		background: #FFDEAD;
		margin-left: 500px;
		padding: 20px;
		position: absolute;
	}
</style>
<body>
	<aside>
		<img src="resources/quote.jpg" alt="quote" class="logo">
		<div class="menu">
			<a href="<%= Pages.USER_PAGE %>">MAIN PAGE</a>
			<a href="<%= Pages.USER_ALL_GYMS %>"><h4>ALL GYMS</h4></a>
			<a href="<%= Pages.SCHEDULE %>"><h4>SCHEDULE</h4></a>
			<a href="<%= Pages.USER_DEPOSIT_MONEY %>"><h4>DEPOSIT MONEY</h4></a>
			<a href="<%= Pages.USER_LOGOUT %>"><h4>LOGOUT</h4></a>
		</div>
	</aside>
	
	<main>
		<div class="deposit_money">
			<p style="color: red"><c:out value="${requestScope.error}"/></p> <br>
			<c:forEach var="error" items="${form.formErrors}">
				<h3 style="color: red;"> ${error} </h3>
			</c:forEach>
			<form action="<%= Pages.USER_DEPOSIT_MONEY %>" method="post">
				<c:forEach var="field" items="${form.visibleFields}">
					<label for="${field.name}">${field.label}</label>
					<input type="${field.type}" name="${field.name}" id="${field.name}" value="${field.value}">
					<p style="color: red">${field.error}</p>
				</c:forEach>
  				<input type="submit" class="btn_logreg" name="btn_money" value="Deposit">
			</form>
			<c:if test="${requestScope.success_message != null}">
				<div class="alert alert-success" role="alert">
					<c:out value=" ${requestScope.success_message} " />
				</div>
			</c:if>
		</div>
	</main>
</body>
</html>