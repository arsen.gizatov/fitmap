<%@ page import="common.Pages" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="resources/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
	<script charset="UTF-8" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<title>Document</title>
</head>
<body>
	<form class="main_form" method="post" action="<%= Pages.USER_LOGIN %>">
		<p style="color: red"><c:out value="${requestScope.error}"/></p> <br>
		<c:forEach var="error" items="${form.formErrors}">
			<h3 style="color: red;"> ${error} </h3>
		</c:forEach>
		<div class="imgcontainer">
			<img src="resources/quote.jpg" alt="quote" class="quote">
		</div>
		
		<div class="logcontainer">
			<c:forEach var="field" items="${form.visibleFields}">
				<label for="${field.name}">${field.label}</label>
				<input type="${field.type}" name="${field.name}" id="${field.name}" value="${field.value}">
				<p style="color: red">${field.error}</p>
			</c:forEach>
		    <input type="submit" class="btn_logreg" name="btn_login" value="Login"><br><br>
		    <a href="<%= Pages.USER_REGISTRATION %>">Registration</a>
		</div>
	</form>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>