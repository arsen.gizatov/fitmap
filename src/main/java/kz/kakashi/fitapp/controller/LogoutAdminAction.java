package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class LogoutAdminAction extends Action {
    @Override
    public String getName() {
        return Pages.ADMIN_LOGOUT;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute(Session.ADMIN);
        return Pages.ADMIN_LOGIN;
    }
}
