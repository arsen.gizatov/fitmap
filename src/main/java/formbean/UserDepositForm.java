package formbean;

import lombok.Data;
import org.formbeanfactory.FieldOrder;
import org.formbeanfactory.FormBean;

@Data
@FieldOrder("cardNumber,securityCode,money")
public class UserDepositForm extends FormBean {
    private String cardNumber;
    private String securityCode;
    private String money;

    public double getMoneyAsDouble() {
        return Double.parseDouble(money);
    }

    @Override
    public void validate() {
        super.validate();

        if(hasValidationErrors())
            return;

        if (cardNumber.length() != 16)
            addFieldError("cardNumber", "Invalid Format");
        if (securityCode.length() != 3)
            addFieldError("securityCode", "Invalid Format");

        try {
            Double.parseDouble(money);
        } catch (NumberFormatException ex) {
            addFieldError("money", "Money should be a decimal number");
        }
    }
}
