package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import databean.User;
import databean.UserCredentials;
import formbean.UserLoginForm;
import model.Model;
import model.UserCredentialsDao;
import model.UserDao;
import org.formbeanfactory.FormBeanFactory;
import org.formbeanfactory.FormBeanFactoryException;
import org.genericdao.MatchArg;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UserLoginAction extends Action{
    private FormBeanFactory<UserLoginForm> formFormBeanFactory = new FormBeanFactory<>(UserLoginForm.class);
    private final UserCredentialsDao userCredentialsDao;
    private UserDao userDao;

    public UserLoginAction(Model model) {
        this.userCredentialsDao = model.getUserCredentialsDao();
        this.userDao = model.getUserDao();
    }

    @Override
    public String getName() {
        return Pages.USER_LOGIN;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute(Session.USER) != null)
            return Pages.USER_PAGE;
        request.setAttribute("form", new UserLoginForm());
        return Pages.JSP_USER_LOGIN;
    }

    @Override
    public String performPost(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute(Session.USER) != null)
            return Pages.USER_PAGE;

        try {
            UserLoginForm form = formFormBeanFactory.create(request);
            request.setAttribute("form", form);

            if (form.hasValidationErrors())
                return Pages.JSP_USER_LOGIN;

            UserCredentials userCredentials = userCredentialsDao.read(form.getEmail());
            if (userCredentials == null) {
                form.addFieldError("email", "No such email!");
                return Pages.JSP_USER_LOGIN;
            }

            if (!userCredentials.getPassword().equals(form.getPassword())) {
                form.addFieldError("password", "Wrong password!");
                return Pages.JSP_USER_LOGIN;
            }

            User currentUser = userDao.match(MatchArg.equals("email", form.getEmail()))[0];
            session.setAttribute(Session.USER, currentUser);

            return Pages.USER_PAGE;
        } catch (RollbackException | FormBeanFactoryException ex) {
            request.setAttribute("error", ex.getMessage());
            return Pages.JSP_USER_LOGIN;
        }
    }
}
