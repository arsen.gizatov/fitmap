package model;

import databean.AdminUsers;
import org.genericdao.ConnectionPool;
import org.genericdao.DAOException;
import org.genericdao.GenericDAO;

public class AdminUsersDao extends GenericDAO<AdminUsers> {
    public AdminUsersDao(ConnectionPool connectionPool, String tableName) throws DAOException {
        super(AdminUsers.class, tableName, connectionPool);
    }

    public static final String TABLE_NAME = "admin_users";
}
