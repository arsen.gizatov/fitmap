<%@ page import="common.Pages" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="resources/style.css">
	<title>Document</title>
</head>
<body>
	<aside>
		<img src="resources/quote.jpg" alt="quote" class="logo">
		<div class="menu">
			<a href="<%= Pages.USER_PAGE %>">MAIN PAGE</a>
			<a href="<%= Pages.USER_ALL_GYMS %>"><h4>ALL GYMS</h4></a>
			<a href="<%= Pages.SCHEDULE %>"><h4>SCHEDULE</h4></a>
			<a href="<%= Pages.USER_DEPOSIT_MONEY %>"><h4>DEPOSIT MONEY</h4></a>
			<a href="<%= Pages.USER_LOGOUT %>"><h4>LOGOUT</h4></a>
		</div>
	</aside>
	
	<main>
		<p style="color: red"><c:out value="${requestScope.error}"/></p> <br>
		<c:forEach var="error" items="${form.formErrors}">
			<h3 style="color: red;"> ${error} </h3>
		</c:forEach>

		<div class="schedule">
			<h3>Schedule</h3>
			<table style="width: 90%;">
			  <tr>
			    <th>Залдың аты</th>
			    <th>Спорт түрі</th>
			    <th>Адрес</th>
			    <th>Күні</th>
			    <th>Уақыт</th>
			    <th>Бағасы</th>
			  	<th></th>
			  </tr>
			  <c:forEach var="schedule" items="${requestScope.schedules}">
				  <tr>
					  <td>${schedule.gym}</td>
					  <td>${schedule.typeOfSport}</td>
					  <td>${schedule.address}</td>
					  <td>${schedule.date}</td>
					  <td>${schedule.time}</td>
					  <td>${schedule.price}</td>
					  <td>
						  <form action="<%= Pages.USER_TAKE_CLASS %>" method="POST">
							  <input type="hidden" name="id" value="${schedule.id}">
							  <button class="btn btn-primary" type="submit">Take Class</button>
						  </form>
					  </td>
				  </tr>
			  </c:forEach>
			</table>
			<c:if test="${requestScope.success_message != null}">
				<div class="alert alert-success" role="alert" style="margin: 20px;">
					<c:out value=" ${requestScope.success_message} " />
				</div>
			</c:if>
			<c:if test="${requestScope.error_message != null}">
				<div class="alert alert-danger" role="alert" style="margin: 20px;">
					<c:out value=" ${requestScope.error_message} " />
				</div>
			</c:if>
		</div>
	</main>
</body>
</html>