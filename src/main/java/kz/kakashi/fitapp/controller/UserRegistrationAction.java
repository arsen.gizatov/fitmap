package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import databean.User;
import databean.UserCredentials;
import formbean.UserRegistrationForm;
import model.Model;
import model.UserCredentialsDao;
import model.UserDao;
import org.formbeanfactory.FormBeanFactory;
import org.formbeanfactory.FormBeanFactoryException;
import org.genericdao.MatchArg;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Objects;

public class UserRegistrationAction extends Action{
    private final double DEFAULT_BALANCE = 0;

    private FormBeanFactory<UserRegistrationForm> formFormBeanFactory = new FormBeanFactory<>(UserRegistrationForm.class);
    private UserCredentialsDao userCredentialsDao;
    private UserDao userDao;

    public UserRegistrationAction(Model model) {
        this.userCredentialsDao = model.getUserCredentialsDao();
        this.userDao = model.getUserDao();
    }

    @Override
    public String getName() {
        return Pages.USER_REGISTRATION;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute(Session.USER) != null)
            return Pages.USER_PAGE;

        request.setAttribute("form", new UserRegistrationForm());
        return Pages.JSP_USER_REGISTRATION;
    }

    @Override
    public String performPost(HttpServletRequest request) {
        HttpSession session = request.getSession();
        try {
            UserRegistrationForm form = formFormBeanFactory.create(request);
            request.setAttribute("form", form);

            if (form.hasValidationErrors())
                return Pages.JSP_USER_REGISTRATION;

            // if validation is ok then validate from db
            UserCredentials userCredentialsFromDb = userCredentialsDao.read(form.getEmail());
            if (userCredentialsFromDb != null) {
                form.addFieldError("email", "Email is already exist!");
                return Pages.JSP_USER_REGISTRATION;
            }
            UserCredentials userCredentials = createUserCredentials(form);
            User user                       = createUser(form);

            this.userDao.create(user);
            this.userCredentialsDao.create(userCredentials);

            request.setAttribute("success_message", "User: " + user.getName() + " created successfully!");
            return Pages.JSP_USER_REGISTRATION;
        } catch (RollbackException | FormBeanFactoryException ex) {
            request.setAttribute("error", ex.getMessage());
            return Pages.JSP_USER_REGISTRATION;
        }
    }

    /**
     *
     * @param form
     * @return new user Entity for insert into db
     */
    private UserCredentials createUserCredentials(UserRegistrationForm form) {
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setEmail(form.getEmail());
        userCredentials.setPassword(form.getPassword());

        return userCredentials;
    }

    /**
     *
     * @param form
     * @return new UserCredential entity for insert into db
     */
    private User createUser(UserRegistrationForm form) {
        User user = new User();
        user.setName(form.getName());
        user.setSurname(form.getSurname());
        user.setCity(form.getCity());
        user.setEmail(form.getEmail());
        user.setBalance(DEFAULT_BALANCE);
        return user;
    }
}
