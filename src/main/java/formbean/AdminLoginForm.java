package formbean;

import lombok.Data;
import org.formbeanfactory.FieldOrder;
import org.formbeanfactory.FormBean;
import org.formbeanfactory.InputType;
import org.formbeanfactory.Label;

@Data
@FieldOrder("gymId,password")
public class AdminLoginForm extends FormBean {
    private String gymId;
    private String password;

    @Label("Gym ID")
    public void setGymId(String gymId) {
        this.gymId = gymId;
    }

    @InputType("password")
    public void setPassword(String password) {
        this.password = password;
    }

    public int getGymIdAsInt() {
        return Integer.parseInt(gymId);
    }
    @Override
    public void validate() {
        super.validate();
        if (hasValidationErrors())
            return;

        try {
            Integer.parseInt(gymId);
        } catch (NumberFormatException ex) {
            addFieldError("gymId", "Id should be a decimal number");
        }
    }
}
