package databean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.genericdao.PrimaryKey;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PrimaryKey("id")
public class Schedules {
    private int id;
    private String gym;
    private String typeOfSport;
    private String address;
    private String date;
    private String time;
    private double price;
}
