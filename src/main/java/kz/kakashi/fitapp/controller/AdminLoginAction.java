package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import databean.AdminCredentials;
import databean.AdminUsers;
import formbean.AdminLoginForm;
import model.AdminCredentialsDao;
import model.AdminUsersDao;
import model.Model;
import org.formbeanfactory.FormBeanFactory;
import org.formbeanfactory.FormBeanFactoryException;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AdminLoginAction extends Action{
    private FormBeanFactory<AdminLoginForm> formFormBeanFactory = new FormBeanFactory<>(AdminLoginForm.class);
    private final AdminCredentialsDao adminCredentialsDao;
    private final AdminUsersDao adminUsersDao;

    public AdminLoginAction(Model model) {
        this.adminCredentialsDao = model.getAdminCredentialsDao();
        this.adminUsersDao = model.getAdminUsersDao();
    }

    @Override
    public String getName() {
        return Pages.ADMIN_LOGIN;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute(Session.ADMIN) != null)
            return Pages.ADMIN_PAGE;
        request.setAttribute("form", new AdminLoginForm());
        return Pages.JSP_ADMIN_LOGIN;
    }

    @Override
    public String performPost(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute(Session.ADMIN) != null)
            return Pages.ADMIN_PAGE;

        try {
            AdminLoginForm form = formFormBeanFactory.create(request);
            request.setAttribute("form", form);

            if (form.hasValidationErrors())
                return Pages.JSP_ADMIN_LOGIN;

            AdminCredentials adminCredentials = adminCredentialsDao.read(form.getGymIdAsInt());
            if (adminCredentials == null) {
                form.addFieldError("gymId", "No such Gym ID!");
                return Pages.JSP_ADMIN_LOGIN;
            }

            if (!adminCredentials.getPassword().equals(form.getPassword())) {
                form.addFieldError("password", "Wrong password!");
                return Pages.JSP_ADMIN_LOGIN;
            }

            AdminUsers currentAdmin = adminUsersDao.read(form.getGymIdAsInt());
            session.setAttribute(Session.ADMIN, currentAdmin);

            return Pages.ADMIN_PAGE;
        } catch (RollbackException | FormBeanFactoryException ex) {
            request.setAttribute("error", ex.getMessage());
            return Pages.JSP_ADMIN_LOGIN;
        }
    }
}
