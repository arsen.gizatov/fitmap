package databean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.genericdao.PrimaryKey;

@Data
@AllArgsConstructor
@NoArgsConstructor
@PrimaryKey("email")
public class UserCredentials {
    private String email;
    private String password;
}
