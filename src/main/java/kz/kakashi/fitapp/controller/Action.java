package kz.kakashi.fitapp.controller;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public abstract class Action {
    /**
     * Returns the name of the action, used to match the request in the map
     * @return the name of action
     */
    public abstract String getName();

    /**
     *
     * @param request
     * @return the name of view which displays the errorMessage
     * not found page
     */
    public String performGet(HttpServletRequest request) {
        request.setAttribute("message",
                "No implementation of performGet() for action: " + getName());
        return "action-error.jsp";
    }

    public String performPost(HttpServletRequest request) {
        request.setAttribute("message",
                "No implementation of performPost() for action: " + getName());
        return "action-error.jsp";
    }

    private static Map<String, Action> hash = new HashMap<>();

    public static void add(Action a) {
        synchronized (hash) {
            if (hash.get(a.getName()) != null) {
                throw new AssertionError("Two actions with the same name ("
                        + a.getName() + "): " + a.getClass().getName()
                        + " and " + hash.get(a.getName()).getClass().getName());
            }

            hash.put(a.getName(), a);
        }
    }

    public static String perform(String name, HttpServletRequest request) {
        Action action;
        synchronized (hash) {
            action = hash.get(name);
        }

        if (action == null) {
            request.setAttribute("message",
                    "There is no action registered for \"" + name + "\"");
            return "action-error.jsp";
        }

        if ("GET".equals(request.getMethod()))
            return action.performGet(request);
        if ("POST".equals(request.getMethod()))
            return action.performPost(request);

        // if any not registered method will come then, give error
        request.setAttribute("message",
                "Unexpected HTTP Method (\"" + request.getMethod() + "\" for \"" + name + "\"");
        return "action.error.jsp";
    }
}
