package model;

import databean.Schedules;
import org.genericdao.ConnectionPool;
import org.genericdao.DAOException;
import org.genericdao.GenericDAO;

public class UserSchedulesDao extends GenericDAO<Schedules> {
    public UserSchedulesDao(ConnectionPool connectionPool, String tableName) throws DAOException {
        super(Schedules.class, tableName, connectionPool);
    }

    public static final String TABLE_NAME = "schedules";
}
