# FitMap fitness management application

### Editors: Gizatov Arsen, Dzhuzmagambetov Alisher, Aidar Tomiris

This application created in Intellij Idea. So, clone repository and open with Intellij IDEA.

#### For database we used MySql
To build local database 
`$ docker-compose up -d `

To run, first of all configure Tomcat server!
