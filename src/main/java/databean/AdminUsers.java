package databean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.genericdao.PrimaryKey;

@Data
@NoArgsConstructor
@AllArgsConstructor
@PrimaryKey("gymId")
public class AdminUsers {
    private int gymId;
    private String name;
    private String address;
    private String type;
    private String contact;
    private double deposit;
}
