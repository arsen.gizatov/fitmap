<%@ page import="common.Pages" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="resources/style.css">
	<title>Document</title>
</head>
<body>
	<button id="btn_admin"><a href="https://www.google.com/" style="color: inherit;text-decoration: none;">Admin</a></button>
	<form class="reg_form" action="<%= Pages.USER_REGISTRATION %>" method="post">
		<div class="imgcontainer">
			<img src="resources/quote.jpg" alt="quote" class="quote">
		</div>
		<p style="color: red"><c:out value="${requestScope.error}"/></p> <br>
		<c:forEach var="error" items="${form.formErrors}">
			<h3 style="color: red;"> ${error} </h3>
		</c:forEach>
		<div class="regcontainer">
			<c:forEach var="field" items="${form.visibleFields}">
				<label for="${field.name}">${field.label}</label>
				<input type="${field.type}" name="${field.name}" id="${field.name}" value="${field.value}">
				<p style="color: red">${field.error}</p>
			</c:forEach>
		    <input type="submit" class="btn_logreg" name="btn_regus" value="Registration"><br><br>
		</div>
		<a href="<%= Pages.USER_LOGIN %>">Sign in</a>
		<c:if test="${requestScope.success_message != null}">
			<div class="alert alert-success" role="alert">
				<c:out value=" ${requestScope.success_message} " />
			</div>
		</c:if>
	</form>
</body>
</html>