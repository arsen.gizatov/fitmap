<%@ page import="common.Pages" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="resources/style.css">
	<title>Document</title>
</head>
<body>
	<aside>
		<img src="resources/quote.jpg" alt="quote" class="logo">
		<div class="menu">
			<h4><a href="<%= Pages.ADMIN_PAGE %>" style="color: inherit;text-decoration: none;">MAIN PAGE</a></h4>
			<h4><a href="<%= Pages.ADMIN_CREATE_CLASSES %>" style="color: inherit;text-decoration: none;">CREATE CLASSES</a></h4>
			<h4><a href="<%= Pages.ADMIN_LOGOUT %>" style="color: inherit;text-decoration: none;">LOGOUT</a></h4>
		</div>
	</aside>
	
	<main>
		<div class="create_class">
			<h3>Create Class</h3>
			<p style="color: red"><c:out value="${requestScope.error}"/></p> <br>
			<c:forEach var="error" items="${form.formErrors}">
				<h3 style="color: red;"> ${error} </h3>
			</c:forEach>
			<form action="<%= Pages.ADMIN_CREATE_CLASSES %>" method="post">
				<label for="fgymname">Gym Name</label>
  				<input type="text" id="fgymname" name="fgymname" value="${sessionScope.admin_user.name}" readonly>
  				<label for="faddress">Address</label>
  				<input type="text" id="faddress" name="faddress" value="${sessionScope.admin_user.address}" readonly>
				<c:forEach var="field" items="${form.visibleFields}">
					<label for="${field.name}">${field.label}</label>
					<input type="${field.type}" name="${field.name}" id="${field.name}" value="${field.value}">
					<p style="color: red">${field.error}</p>
				</c:forEach>
  				<input type="submit" class="btn_logreg" name="btn_create" value="Create">
			</form>

			<c:if test="${requestScope.success_message != null}">
				<div class="alert alert-success" role="alert">
					<c:out value=" ${requestScope.success_message} " />
				</div>
			</c:if>
		</div>
	</main>
</body>
</html>