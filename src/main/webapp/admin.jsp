<%@ page import="common.Pages" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="resources/style.css">
	<title>Document</title>
</head>
<body>
	<aside>
		<img src="resources/quote.jpg" alt="quote" class="logo">
		<div class="menu">
			<h4><a href="<%= Pages.ADMIN_PAGE %>>" style="color: inherit;text-decoration: none;">MAIN PAGE</a></h4>
			<h4><a href="<%= Pages.ADMIN_CREATE_CLASSES %>" style="color: inherit;text-decoration: none;">CREATE CLASSES</a></h4>
			<h4><a href="<%= Pages.ADMIN_LOGOUT %>" style="color: inherit;text-decoration: none;">LOGOUT</a></h4>
		</div>
	</aside>
	
	<main>
		<div class="user_image">
			<img alt="myphoto" src="resources/fitnes.jpg" width="250px" height="230px">
			<h4>${requestScope.admin.name}</h4>
		</div>

		<div class="user_info">
			<h3>My Info</h3>
			<form>
				<label for="fgymname">Gym Name</label>
  				<input type="text" id="fgymname" name="fgymname" value="${requestScope.admin.name}" disabled>
  				<label for="faddress">Address</label>
  				<input type="text" id="faddress" name="faddress" value="${requestScope.admin.address}" disabled>
  				<label for="ftypeclass">Types of classes</label>
  				<input type="text" id="ftypeclass" name="ftypeclass" value="${requestScope.admin.type}" disabled>
  				<label for="fcontact">Contact</label>
  				<input type="text" id="fcontact" name="fcontact" value="${requestScope.admin.contact}" disabled>
				<label for="fdeposit">Deposit</label>
				<input type="text" id="fdeposit" name="fdeposit" value="${requestScope.admin.deposit}" disabled>
			</form>
		</div>
	</main>
</body>
</html>