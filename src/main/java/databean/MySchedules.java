package databean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.genericdao.PrimaryKey;

@Data
@NoArgsConstructor
@AllArgsConstructor
@PrimaryKey("id")
public class MySchedules {
    private int id;
    private int scheduleId;
    private String gym;
    private String typeOfSport;
    private String address;
    private String date;
    private String time;
}
