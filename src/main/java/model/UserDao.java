package model;

import databean.User;
import org.genericdao.ConnectionPool;
import org.genericdao.DAOException;
import org.genericdao.GenericDAO;

public class UserDao extends GenericDAO<User> {
    public UserDao(ConnectionPool connectionPool, String tableName) throws DAOException {
        super(User.class, tableName, connectionPool);
    }

    public static final String TABLE_NAME = "main_users";
}
