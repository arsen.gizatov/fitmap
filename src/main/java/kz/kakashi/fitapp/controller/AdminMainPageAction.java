package kz.kakashi.fitapp.controller;

import common.Pages;
import common.Session;
import databean.AdminUsers;
import model.AdminUsersDao;
import model.Model;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AdminMainPageAction extends Action {
    private AdminUsersDao adminUsersDao;

    public AdminMainPageAction(Model model) {
        this.adminUsersDao = model.getAdminUsersDao();
    }

    @Override
    public String getName() {
        return Pages.ADMIN_PAGE;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        HttpSession session = request.getSession();

        AdminUsers currentAdmin = (AdminUsers) session.getAttribute(Session.ADMIN);
        try {
            AdminUsers adminFromDb = adminUsersDao.read(currentAdmin.getGymId());
            request.setAttribute("admin", adminFromDb);
            return Pages.JSP_ADMIN_PAGE;
        } catch (RollbackException exception) {
            request.setAttribute("error", exception.getMessage());
            return Pages.JSP_ADMIN_PAGE;
        }
    }
}
