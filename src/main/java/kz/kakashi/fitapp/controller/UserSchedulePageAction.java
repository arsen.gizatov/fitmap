package kz.kakashi.fitapp.controller;

import common.Pages;
import databean.Schedules;
import model.Model;
import model.UserSchedulesDao;
import org.genericdao.RollbackException;

import javax.servlet.http.HttpServletRequest;

public class UserSchedulePageAction extends Action{
    private UserSchedulesDao userSchedulesDao;

    public UserSchedulePageAction(Model model) {
        this.userSchedulesDao = model.getUserSchedulesDao();
    }

    @Override
    public String getName() {
        return Pages.SCHEDULE;
    }

    @Override
    public String performGet(HttpServletRequest request) {
        try {
            Schedules[] allSchedules = userSchedulesDao.match();
            request.setAttribute("schedules", allSchedules);
            return Pages.JSP_SCHEDULE;
        } catch (RollbackException ex) {
            request.setAttribute("error", ex.getMessage());
            return Pages.JSP_SCHEDULE;
        }
    }
}
