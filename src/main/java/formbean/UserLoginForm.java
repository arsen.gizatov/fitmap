package formbean;

import lombok.Data;
import org.formbeanfactory.FieldOrder;
import org.formbeanfactory.FormBean;
import org.formbeanfactory.InputType;

@Data
@FieldOrder("email,password")
public class UserLoginForm extends FormBean {
    private String email;
    private String password;

    @InputType("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void validate() {
        super.validate();
        if (hasValidationErrors())
            return;

        if (!email.matches(".*[@].*"))
            addFieldError("email", "Email format is invalid!");
    }
}
